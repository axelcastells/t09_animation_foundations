﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using RobotLib.Physics;
public class BoxController : MonoBehaviour {

    [Header("Canvas Input Data")]
    public RobotJoint axisJoint1;
    public RobotJoint axisJoint2;
    public RobotJoint axisJoint3;

    public InputField inputFieldAngle1;
    public InputField inputFieldAngle2;
    public InputField inputFieldAngle3;

    public InputField inputFieldAxisX1;
    public InputField inputFieldAxisY1;
    public InputField inputFieldAxisZ1;
    public InputField inputFieldAxisX2;
    public InputField inputFieldAxisY2;
    public InputField inputFieldAxisZ2;
    public InputField inputFieldAxisX3;
    public InputField inputFieldAxisY3;
    public InputField inputFieldAxisZ3;

    public List<Transform> robotAxis = null;
    [Space(20)]

    [Header("Joint Data")]
    public List<Transform> joints = null;
    public List<RobotJoint> robotJoints = null;


    private FKController robotController;
    private List<float> solution = null;

    private List<Quaternion> jointRots = null;

    private bool autoApply;
    public bool isJoint = false;

    private Vector3 axisController1;
    private Vector3 axisController2;
    private Vector3 axisController3;

    // Use this for initialization
    void Start()
    {
        autoApply = false;
        jointRots = new List<Quaternion>();
        robotJoints = new List<RobotJoint>();
        solution = new List<float>();

        for(int i = 0; i < joints.Count - 1; i++)
        {
            solution.Add(0);
            robotJoints.Add(new RobotJoint());
            robotJoints[i].axisConstraint = new RobotLib.Vector3();
            robotJoints[i].zeroEuler = new RobotLib.Vector3(joints[i].transform.localEulerAngles.x, joints[i].transform.localEulerAngles.y, joints[i].transform.localEulerAngles.z);
            jointRots.Add(new Quaternion());
        }
        robotJoints.Add(null);
        jointRots.Add(new Quaternion());

        robotController = new FKController();
        robotController.LinkData(ref robotJoints, ref solution);
    }

    // Update is called once per frame
    void Update () {

    }

    private void FixedUpdate()
    {
        for(int i = 0; i < joints.Count; i++)
        {
            //Vector3 axis;
            //float angle;
            //jointRots[i].ToAngleAxis(out angle, out axis);
            joints[i].localRotation = Quaternion.Slerp(joints[i].transform.localRotation, jointRots[i], Time.deltaTime);

            //robotAxis[i].localRotation = joints[i].localRotation;
        }

        // Robot Joints Update
        for (int i = 0; i < robotAxis.Count; i++)
        {
            Vector3 _axis;
            float _angle;
            joints[i].localRotation.ToAngleAxis(out _angle, out _axis);

            float _newAngle = _angle;
            _newAngle = KinematicsController.ConstraintAngle(robotJoints[i], _newAngle);

            RobotLib.Vector3 _newAxis = new RobotLib.Vector3(_axis.x, _axis.y, _axis.z);
            _newAxis = KinematicsController.ConstraintAxis(_newAxis, robotJoints[i].axisConstraint);

            robotAxis[i].localRotation = Quaternion.AngleAxis(_newAngle, new Vector3(_newAxis.x, _newAxis.y, _newAxis.z));

            //robotAxis[i].localRotation = joints[i].localRotation;
        }
    }

    public void Apply()
    {
        axisController1.x = float.Parse(inputFieldAxisX1.text);
        axisController1.y = float.Parse(inputFieldAxisY1.text);
        axisController1.z = float.Parse(inputFieldAxisZ1.text);
        axisController2.x = float.Parse(inputFieldAxisX2.text);
        axisController2.y = float.Parse(inputFieldAxisY2.text);
        axisController2.z = float.Parse(inputFieldAxisZ2.text);
        axisController3.x = float.Parse(inputFieldAxisX3.text);
        axisController3.y = float.Parse(inputFieldAxisY3.text);
        axisController3.z = float.Parse(inputFieldAxisZ3.text);

        solution[0] = -float.Parse(inputFieldAngle1.text) * Mathf.Deg2Rad;
        solution[1] = -float.Parse(inputFieldAngle2.text) * Mathf.Deg2Rad;
        solution[2] = -float.Parse(inputFieldAngle3.text) * Mathf.Deg2Rad;

        robotJoints[0] = new RobotJoint(axisController1.x, axisController1.y, axisController1.z, 0, 1, 0, 0, 90);
        robotJoints[1] = new RobotJoint(axisController2.x, axisController2.y, axisController2.z, 1, 0, 0, 0, 90);
        robotJoints[2] = new RobotJoint(axisController3.x, axisController3.y, axisController3.z, 1, 0, 0, 0, 90);
        robotJoints[3] = new RobotJoint();


        robotController.Run();

        // Controller Joints Update
        for (int i = 0; i < robotJoints.Count; i++)
        {
           // Debug.Log("Angle: " + robotJoints[i].GetAxisAngle().angle * Mathf.Rad2Deg + " Axis: " + robotJoints[i].GetAxisAngle().x + " " + robotJoints[i].GetAxisAngle().y + " " + robotJoints[i].GetAxisAngle().z);

            jointRots[i] = Quaternion.AngleAxis(robotJoints[i].GetAxisAngle().angle * Mathf.Rad2Deg, new Vector3(robotJoints[i].GetAxisAngle().x, robotJoints[i].GetAxisAngle().y, robotJoints[i].GetAxisAngle().z));


        }


        string report1 = "";
        string report2 = "";
        for(int i = 0; i < robotJoints.Count -1; i++)
        {
            Vector3 _axis;
            float _angle;
            jointRots[i].ToAngleAxis(out _angle, out _axis);

            float _newAngle = _angle;
            _newAngle = KinematicsController.ConstraintAngle(robotJoints[i], _newAngle);

            RobotLib.Vector3 _newAxis = new RobotLib.Vector3(_axis.x, _axis.y, _axis.z);
            _newAxis = KinematicsController.ConstraintAxis(_newAxis, robotJoints[i].axisConstraint);

            report1 += "Control " + (i + 1) + "-turnme!:" + jointRots[i] + "\n";
            report2 += "Joint" + (i + 1) + ":" + Quaternion.AngleAxis(_newAngle, new Vector3(_newAxis.x, _newAxis.y, _newAxis.z)) + "\n";
            //if (isJoint == false)
            //    Debug.Log("control" + (i+1) + "-turnme!:" + joints[i].rotation + "\n");

            //else
            //    Debug.Log("Joint" + (i+1) + ":" + robotAxis[i].rotation);

        }
        Debug.Log(report1 + report2);
    }
}
