﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Exercice2 : MonoBehaviour {

    public Transform myTarget;
    public GameObject footBall;
    private Vector3 ballPos;
    public Slider angleValue;
    public GameObject redArrow;
    public GameObject greenArrow;
    private float shootAngle;

	// Use this for initialization
	void Start () {
        ballPos = transform.position;
	}

	// Update is called once per frame
	void Update () {

        shootAngle = angleValue.value;

        redArrowControl();
        greenArrowControl();

        if (Input.GetKeyDown("space"))
        { 
            footBall.GetComponent<Rigidbody>().velocity = BallVelocity(myTarget, shootAngle);
            
        }
	}

    public Vector3 BallVelocity(Transform target, float angle)
    {
        Vector3 dir = target.position - transform.position;
        float height = dir.y;
        dir.y = 0f;
        float dist = dir.magnitude;
        float a = angle * Mathf.Deg2Rad;
        dir.y = dist * Mathf.Tan(a);
        dist += height / Mathf.Tan(a);

        float velocity = Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a));
        return velocity * dir.normalized;
    }

    private void redArrowControl()
    {
        redArrow.transform.LookAt(myTarget);
        redArrow.transform.localScale = new Vector3(redArrow.transform.localScale.x, redArrow.transform.localScale.y, shootAngle/1000);
    }

    private void greenArrowControl()
    {
        greenArrow.transform.LookAt(myTarget);
        greenArrow.transform.localScale = new Vector3(greenArrow.transform.localScale.x, greenArrow.transform.localScale.y, footBall.GetComponent<Rigidbody>().velocity.magnitude);
    }

    private void OnTriggerEnter(Collider other)
    {
        footBall.transform.position = ballPos;
        footBall.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

    }
     
}
