﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Master : MonoBehaviour {

    public List<Animator> animators;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyDown(KeyCode.A)){
            foreach (Animator a in animators)
            {
                a.SetTrigger("Run");
            }
        }
	}

}
