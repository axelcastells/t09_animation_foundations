﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusController : MonoBehaviour {

    public List<Transform> tentacles;
    public Transform highlighter;
    public Transform target;

    private int highlightedTentacleIndex;

	// Use this for initialization
	void Start () {
        highlightedTentacleIndex = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            float dist = -1;
            int distIndex = -1;
            for(int i = 0; i < tentacles.Count; i++)
            {
                if ((dist > Vector3.Distance(tentacles[i].GetComponent<TentacleController>().GetEffector().position, target.position)) || dist == -1)
                {
                    dist = Vector3.Distance(tentacles[i].GetComponent<TentacleController>().GetEffector().position, target.position);
                    distIndex = i;
                }
            }

            highlightedTentacleIndex = distIndex;

            for (int i = 0; i < tentacles.Count; i++)
            {
                if (i == highlightedTentacleIndex) tentacles[i].GetComponent<TentacleController>().active = true;
                else tentacles[i].GetComponent<TentacleController>().active = false;
            }

        }

        if(tentacles[highlightedTentacleIndex].GetComponent<TentacleController>().active)
            highlighter.position = tentacles[highlightedTentacleIndex].GetComponent<TentacleController>().GetEffector().position;
	}
}
