﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RobotLib.Physics;

public class TentacleController : MonoBehaviour {

    public bool active;

    public Transform target;
    public Transform baseJoint;

    public List<Transform> joints;

    private IKCCDSolver controller;


	// Use this for initialization
	void Start () {
        active = false;
        controller = new IKCCDSolver();

        foreach (Transform t in baseJoint.GetComponentsInChildren<Transform>())
        {
            joints.Add(t);
        }

        UpdateJoints();

        controller.Start();
    }

    private void Update()
    {
        if (active)
        {
            UpdateJoints();

            controller.targ = new RobotLib.Vector3(target.position.x, target.position.y, target.position.z);
            controller.Update();

            for (int i = 0; i < joints.Count; i++)
            {
                joints[i].rotation = Quaternion.AngleAxis(controller.joints[i].GetRotation().angle, new Vector3(   controller.joints[i].GetRotation().x, 
                                                                                                                    controller.joints[i].GetRotation().y, 
                                                                                                                    controller.joints[i].GetRotation().z)) * joints[i].rotation;
            }
        }


    }

    void UpdateJoints()
    {
        controller.joints = new List<RobotLib.Transform>();
        foreach (Transform j in joints)
        {
            RobotLib.Transform t = new RobotLib.Transform();
            t.position = new RobotLib.Vector3(j.position.x, j.position.y, j.position.z);

            float angle;
            Vector3 axis;
            j.rotation.ToAngleAxis(out angle, out axis);

            t.SetRotation(new RobotLib.AxisAngle(new RobotLib.Vector3(axis.x, axis.y, axis.z), angle));
            controller.joints.Add(t);
        }
    }

    // Update is called once per frame
    public Transform GetEffector()
    {
        return joints[joints.Count - 1];
    }

    void Debugger()
    {
        Debug.Log("Finished CCD!");
    }
}
