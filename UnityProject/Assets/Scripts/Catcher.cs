﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Catcher : MonoBehaviour {

    public Animator animator;
    public Transform handSlot;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "DynamicObject")
        {
            animator.SetTrigger("Run");
            other.transform.SetParent(handSlot);
        }
            
        
    }
}
