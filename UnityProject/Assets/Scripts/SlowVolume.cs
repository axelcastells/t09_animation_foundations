﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class SlowVolume : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Enter Collision!");
        Time.timeScale = .3f;
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Exit Collision!");
        Time.timeScale = 1;
    }
}
