# **T09_Animation_Foundations**

## _**Team Description**_

-Team: T09

-Names: 
   
    Axel Castells 
    
    David López

![alt text](img/DSC00149.jpg)
![alt text](img/201810261704avatar.png)


-Email: axelcatellsmonllau@enti.cat, davidlopezsaludes@enti.cat


## _**Delivery 1**_

### _**Exercice 1**_

![alt text](img/Characters.PNG)

### _**Exercicie 2**_
We put our character on T pose to fit with the animation; we characterized it and plotted it, 
afterwards we did the same with the other character putting both on the animation layer, and fixed the animation using keyframes. 
The difficulties found were our lack of experience using Motion Builder.
![alt text](img/HandShake-Gif.gif)

### _**Exercice 3**_
We created the camera effect by animating the camera, and using a collider to reduce the global time scale.
When the collider detects something the time lapse is created, when there's nothing the time goes back to normal.
The problems to input the animations were the only problems found.
![alt text](img/HandShakeUnityr-Gif.gif)

### _**Exercicie 4**_
The only difficulties found were that some characters when merged made Motion Builder change some outputs such as the timeline. 
We used the same method of exercice 2, with the difference that we used the already animated characters to plot with the new characters.
![alt text](img/HandShake4char-Gif.gif)

### _**Exercice 5**_
The main difficulty found on this exercise was the lack of time, instead of using the key B we used the key A. We just used the same method as exercice 3 for the time lapse.
Due to some exporting problem the teddy does not move as we animated it.
![alt text](img/HandShake4Unityr-Gif.gif)

### _**Exercicie 6**_
We could not finish this exercise, we only could implement the animation in Motion Builder by changing the keys of the handshake animation. 
No difficulties were found.
![alt text](img/HandPick-Gif.gif)

## _**Delivery 2**_

### _**Exercice 1**_
-1: When we implemented the class MyQuaternion, the first methods implemented are the constructors of the class
and the destructor. Then there is the Normalize method, in which we calculate the properties (x, y, z) magnitude of the Quaternion class 
and the we divide the quaternion properties (w, x, y, z) by the magnitude. Then we overload the operators + and * in order to multiply or sum two quaternions,
we also overload the operator * again in order to multiply a quaternion by a vector. We also implemented the method inverse, which just returns the inverse of the 
quaternion. Finally we implemented the methods AxisAngleToQuaternion and QuaternionToAxisAngle which convert an axis and an angle to a quaternion and vice versa.

-2: 
![alt text](img/Capture.PNG)

-3:
![alt text](img/Capture2.PNG)

### _**Exercicie 2**_

-1: We did the requierments inverse, we first did the smooth movement by applying a Slerp to the list where we save the joints of the control_system ogject,
then to make the robotic arm depend on the blue box object we created a list of transforms where we assigned the robot joints as it's values,
and in the same function where we do the slerp we equaled this list to the joints list so it depens on the joints list. For the constrains, thou it doesn't work,
we applied a method on the library that constrains the movement of the arm, we pass the constrains we want in the constructor of the library which we called 
RobotJoint.

-2:
![alt text](img/RoboticArm.gif)

-3:
![alt text](img/ArmrobotCapture.PNG)

## _**Delivery 3**_

### _**Exercice 1**_

-1: Scene on the Scenes folder on the project

-2:Not gif could be provided 

-3:Explanation on the Unity canvas

### _**Exercice 2**_

-1: We calculated the direction of the object by first taking the direction between the target and the ball (target-transform), then we equal the Y component of
the direction to 0 so we will have the vector between the target and the ball dus getting the direction that the ball must follow, then we will apply the angle to 
it's Y component to get the parabola movement product and the strength of the shoot. We take the velocity by calculating the Square Root of the distance per gravity 
divided by the sinus of 2 multiplied by the angle (squrt(distance * gravity/sin(2 * angle))). This calculations are made on the BallVelocity function.

-2:Explanation on the Unity canvas

-3: We make the same calculations that we explained on the first point of the exercice.
![alt text](img/ShootBall.gif)

## _**Re-evaluation**_

### _**Delivery 2**_

The angle and axis constrains have been added and they only apply to the robot joints so the robot doesn't break, the controller joints have no constrains applied.
Constrained and unconstrained quaternions are printed now and they change each time the values are changed and applied and all quaternion debugging is printed on 
a single debug log (must check the extended debug view to see all the content). The Constrained quaternions go as "JointX"(as X is 1,2,3) and the 
unconstrained quaternions go as "control X!-turnme" (as X is 1,2,3). The instructions are also in the Txt.




